# README #

The project consists of an banking app with a single endpoint that is used for creating a savings account in the Monday-Friday, 9-17 interval. 
The date and time interval is reffered to the server time, not the local time. Because of defining the time on the server side, not being received in the request body, mocking the server time for testing purposes was difficult,
so the only way the tests that verify the creation of the account in the time range is by changing your computer time manually

### How do I get set up? ###

* Execute the SQL file, on a MYSQL server with user and password "root".
* Start the server by running the BankProjectApplication.java file
* Import the postman collection in the Postman app and send the only request available from the Postman app. 
* Tests can be run individually or all at once, only the tests that depend on the time will be failing.
