package com.andreicolda.bankproject.repositories;

import com.andreicolda.bankproject.models.Account;
import com.andreicolda.bankproject.models.AccountType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AccountRepository extends JpaRepository<Account, Double>
{
    Account findAccountByIdClientAndAccountType(double id, AccountType accountType);
}
