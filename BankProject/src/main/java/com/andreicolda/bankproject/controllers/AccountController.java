package com.andreicolda.bankproject.controllers;

import com.andreicolda.bankproject.managers.AccountManager;
import com.andreicolda.bankproject.models.Account;
import com.andreicolda.bankproject.models.ui.GenericResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
public class AccountController
{
    @Autowired
    private AccountManager accountManager;

    @RequestMapping(value = "/createSavingsAccount", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public GenericResponse createSavingsAccount(@RequestBody Account account)
    {
        return accountManager.createSavingsAccount(account);
    }
}
