package com.andreicolda.bankproject.models;

public enum Currency
{
    RON,
    EUR,
    USD
}
