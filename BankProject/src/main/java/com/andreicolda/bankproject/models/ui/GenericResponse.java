package com.andreicolda.bankproject.models.ui;


public class GenericResponse
{
    private ReturnState returnState = ReturnState.SUCCESS;
    private Object data;

    public GenericResponse(ReturnState returnState, Object data)
    {
        this.returnState = returnState;
        this.data = data;
    }

    public GenericResponse(ReturnState returnState)
    {
        this.returnState = returnState;
    }

    public GenericResponse(Object data)
    {
        this.data = data;
    }

    public Object getData()
    {
        return data;
    }

    public void setData(Object data)
    {
        this.data = data;
    }

    public int getReturnCode()
    {
        return returnState.getCode();
    }

    public void setReturnCode(int returnCode)
    {

        for (ReturnState state : ReturnState.values())
        {
            if (state.getCode() == returnCode)
            {
                this.returnState = state;
                break;
            }
        }
    }
}