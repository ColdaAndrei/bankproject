package com.andreicolda.bankproject.models;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;


@Entity
public class Account
{
    private double id;
    private String accountNumber;
    private AccountType accountType;
    private Currency currency;
    private Double idClient;
    private Timestamp creationDate;
    private Float balance;
    private boolean enabled;

    @Id
    @Column(name = "id", table = "account")
    public double getId()
    {
        return id;
    }

    public void setId(double id)
    {
        this.id = id;
    }

    @Basic
    @Column(name = "account_number", table = "account")
    public String getAccountNumber()
    {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    @Basic
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "account_type", table = "account")
    public AccountType getAccountType()
    {
        return accountType;
    }

    public void setAccountType(AccountType accountType)
    {
        this.accountType = accountType;
    }


    @Basic
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "currency", table = "account")
    public Currency getCurrency()
    {
        return currency;
    }

    public void setCurrency(Currency accountType)
    {
        this.currency = accountType;
    }

    @Basic
    @Column(name = "id_client", table = "account")
    public Double getIdClient()
    {
        return idClient;
    }

    public void setIdClient(Double idClient)
    {
        this.idClient = idClient;
    }

    @Basic
    @Column(name = "creation_date", table = "account")
    public Timestamp getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate)
    {
        this.creationDate = creationDate;
    }

    @Basic
    @Column(name = "balance", table = "account")
    public Float getBalance()
    {
        return balance;
    }

    public void setBalance(Float balance)
    {
        this.balance = balance;
    }

    @Basic
    @Column(name = "enabled", table = "account", columnDefinition = "TINYINT(1)")
    public boolean getEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Account account = (Account) o;
        return Double.compare(account.id, id) == 0 &&
                enabled == account.enabled &&
                Objects.equals(accountNumber, account.accountNumber) &&
                accountType == account.accountType &&
                Objects.equals(currency, account.currency) &&
                Objects.equals(idClient, account.idClient) &&
                Objects.equals(creationDate, account.creationDate) &&
                Objects.equals(balance, account.balance);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, accountNumber, accountType, currency, idClient, creationDate, balance, enabled);
    }
}
