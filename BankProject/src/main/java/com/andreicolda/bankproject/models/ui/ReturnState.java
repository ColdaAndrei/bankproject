package com.andreicolda.bankproject.models.ui;

public enum ReturnState
{

    SUCCESS(0),
    ERROR(1),

    NOT_IN_TIME_RANGE(100),
    WRONG_INPUT(101),
    CLIENT_NOT_FOUND(102),
    ACCOUNT_LIMIT_EXCEEDED(103);

    private int code;

    ReturnState(int code)
    {
        this.code = code;
    }

    public static ReturnState getReturnStateByCode(int codeParam)
    {

        for (ReturnState returnState : values())
        {

            if (returnState.code == codeParam)
            {
                return returnState;
            }
        }

        return null;
    }

    public int getCode()
    {
        return code;
    }
}