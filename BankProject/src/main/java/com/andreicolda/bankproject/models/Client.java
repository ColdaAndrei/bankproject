package com.andreicolda.bankproject.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;


@Entity
public class Client
{
    private double id;
    private String name;
    private String address;
    private String email;
    private String telephone;
    private String personalIdentifNb;

    @Id
    @Column(name = "id", table = "client")
    public double getId()
    {
        return id;
    }

    public void setId(double id)
    {
        this.id = id;
    }

    @Basic
    @Column(name = "name", table = "client")
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Basic
    @Column(name = "address", table = "client")
    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    @Basic
    @Column(name = "email", table = "client")
    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    @Basic
    @Column(name = "telephone", table = "client")
    public String getTelephone()
    {
        return telephone;
    }

    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }

    @Basic
    @Column(name = "personal_identif_nb", table = "client")
    public String getPersonalIdentifNb()
    {
        return personalIdentifNb;
    }

    public void setPersonalIdentifNb(String personalIdentifNb)
    {
        this.personalIdentifNb = personalIdentifNb;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Client client = (Client) o;

        if (Double.compare(client.id, id) != 0)
        {
            return false;
        }
        if (!Objects.equals(name, client.name))
        {
            return false;
        }
        if (!Objects.equals(address, client.address))
        {
            return false;
        }
        if (!Objects.equals(email, client.email))
        {
            return false;
        }
        if (!Objects.equals(telephone, client.telephone))
        {
            return false;
        }
        if (!Objects.equals(personalIdentifNb, client.personalIdentifNb))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = Double.doubleToLongBits(id);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (telephone != null ? telephone.hashCode() : 0);
        result = 31 * result + (personalIdentifNb != null ? personalIdentifNb.hashCode() : 0);
        return result;
    }
}
