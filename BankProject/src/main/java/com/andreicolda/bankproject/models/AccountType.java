package com.andreicolda.bankproject.models;

public enum AccountType
{
    SAVINGS,
    CHECKING,
    RETIREMENT
}
