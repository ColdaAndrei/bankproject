package com.andreicolda.bankproject.services;

import com.andreicolda.bankproject.models.Account;
import com.andreicolda.bankproject.models.AccountType;
import com.andreicolda.bankproject.models.ui.GenericResponse;
import com.andreicolda.bankproject.models.ui.ReturnState;
import com.andreicolda.bankproject.repositories.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class AccountService
{
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

    @Autowired
    private AccountRepository accountRepository;

    public GenericResponse createSavingsAccount(Account account)
    {
        try
        {
            accountRepository.save(account);
            return new GenericResponse(ReturnState.SUCCESS);
        }
        catch (Exception e)
        {
            logger.warn("Exception caught during saving account operation", e);
            return new GenericResponse(ReturnState.ERROR);
        }
    }

    public Optional<Account> findSavingsAccountForClient(double id)
    {
        return Optional.ofNullable(accountRepository.findAccountByIdClientAndAccountType(id, AccountType.SAVINGS));
    }
}
