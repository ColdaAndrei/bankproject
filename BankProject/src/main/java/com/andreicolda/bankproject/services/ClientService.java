package com.andreicolda.bankproject.services;

import com.andreicolda.bankproject.models.Client;
import com.andreicolda.bankproject.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ClientService
{
    @Autowired
    private ClientRepository clientRepository;

    public Optional<Client> findClientById(double id)
    {
        return clientRepository.findById(id);
    }
}
