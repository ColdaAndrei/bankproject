package com.andreicolda.bankproject.utils;

import com.andreicolda.bankproject.models.Account;
import com.andreicolda.bankproject.models.AccountType;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.Random;


public class AccountUtils
{

    public static boolean checkSavingsAccountTime(LocalDateTime givenTime)
    {
        DayOfWeek creationDay = givenTime.getDayOfWeek();
        LocalDateTime dateWithMinHour = givenTime.withHour(SystemProperties.getWorkingHoursStart());
        LocalDateTime dateWithMaxHour = givenTime.withHour(SystemProperties.getWorkingHoursEnd());

        return creationDay != DayOfWeek.SATURDAY && creationDay != DayOfWeek.SUNDAY &&
                givenTime.isAfter(dateWithMinHour) && givenTime.isBefore(dateWithMaxHour);
    }

    public static boolean validateInput(Account account, AccountType accountType)
    {
        if (account.getAccountType() != accountType)
        {
            return false;
        }

        if (account.getCurrency() == null)
        {
            return false;
        }

        if (account.getIdClient() == null)
        {
            return false;
        }

        if (account.getBalance() == null)
        {
            account.setBalance(0f);
        }
        return true;
    }

    /**
     * @return randomly generated account number, since the account number assigning is not known by me
     */
    public static String createAccountNumber(Account account)
    {
        return account.getCurrency() + String.valueOf(new Random().nextInt());
    }
}
