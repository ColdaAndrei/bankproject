package com.andreicolda.bankproject.utils;

public class SystemProperties
{
    public static final String WORKING_HOURS_START = "working.hours.start";
    public static final int DEFAULT_WORKING_HOURS_START = 9;

    public static final String WORKING_HOURS_END = "working.hours.end";
    public static final int DEFAULT_WORKING_HOURS_END = 17;

    public static int getWorkingHoursStart()
    {
        return Integer.getInteger(WORKING_HOURS_START, DEFAULT_WORKING_HOURS_START);
    }

    public static int getWorkingHoursEnd()
    {
        return Integer.getInteger(WORKING_HOURS_END, DEFAULT_WORKING_HOURS_END);
    }
}
