package com.andreicolda.bankproject.configuration;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;


@Configuration
public class AppConfig
{
    @Bean
    public DataSource dataSource()
    {
        return DataSourceBuilder.create()
                                .driverClassName("com.mysql.jdbc.Driver")
                                .url("jdbc:mysql://localhost:3306/bankdatabase")
                                .username("root")
                                .password("root")
                                .build();
    }
}
