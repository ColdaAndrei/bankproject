package com.andreicolda.bankproject.managers;

import com.andreicolda.bankproject.models.Account;
import com.andreicolda.bankproject.models.ui.GenericResponse;
import com.andreicolda.bankproject.models.ui.ReturnState;
import com.andreicolda.bankproject.services.AccountService;
import com.andreicolda.bankproject.services.ClientService;
import com.andreicolda.bankproject.utils.AccountUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static com.andreicolda.bankproject.models.AccountType.SAVINGS;
import static com.andreicolda.bankproject.models.ui.ReturnState.*;


@Component
public class AccountManager
{
    @Autowired
    private ClientService clientService;
    @Autowired
    private AccountService accountService;

    public GenericResponse createSavingsAccount(Account account)
    {
        LocalDateTime currentTime = getCurrentTime();

        if (!AccountUtils.validateInput(account, SAVINGS))
        {
            return new GenericResponse(WRONG_INPUT);
        }

        if (!AccountUtils.checkSavingsAccountTime(currentTime))
        {
            return new GenericResponse(ReturnState.NOT_IN_TIME_RANGE);
        }

        if (clientService.findClientById(account.getIdClient()).isEmpty())
        {
            return new GenericResponse(CLIENT_NOT_FOUND);
        }

        if (accountService.findSavingsAccountForClient(account.getIdClient()).isEmpty())
        {
            account.setCreationDate(Timestamp.valueOf(currentTime));
            account.setAccountNumber(AccountUtils.createAccountNumber(account));
            return accountService.createSavingsAccount(account);
        }
        else
        {
            return new GenericResponse(ACCOUNT_LIMIT_EXCEEDED);
        }
    }

    public LocalDateTime getCurrentTime()
    {
        return LocalDateTime.now();
    }
}
