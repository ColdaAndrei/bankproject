package com.andreicolda.bankproject.managers;

import com.andreicolda.bankproject.BaseTest;
import com.andreicolda.bankproject.models.*;
import com.andreicolda.bankproject.models.ui.GenericResponse;
import com.andreicolda.bankproject.models.ui.ReturnState;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;


public class AccountManagerTests extends BaseTest
{
    @Autowired
    private AccountManager accountManager;

    @Test
    public void createSavingsAccount_successTest()
    {
        clientRepository.save(client1);

        GenericResponse savingsAccountResult = accountManager.createSavingsAccount(account1);
        Assert.assertEquals(ReturnState.SUCCESS.getCode(), savingsAccountResult.getReturnCode());
    }

    @Test
    public void createSavingsAccount_accountLimitExceededTest()
    {
        clientRepository.save(client1);

        GenericResponse savingsAccountResult = accountManager.createSavingsAccount(account1);
        Assert.assertEquals(ReturnState.SUCCESS.getCode(), savingsAccountResult.getReturnCode());

        Account account2 = new Account();
        account2.setIdClient(1d);
        account2.setAccountType(AccountType.SAVINGS);
        account2.setCurrency(Currency.RON);
        account2.setBalance(0f);
        account2.setAccountNumber("RON1234");

        savingsAccountResult = accountManager.createSavingsAccount(account2);

        Assert.assertEquals(ReturnState.ACCOUNT_LIMIT_EXCEEDED.getCode(), savingsAccountResult.getReturnCode());
    }

    @Test
    public void createSavingsAccount_notInTimeRangeTest()
    {
        clientRepository.save(client1);

        GenericResponse savingsAccountResult = accountManager.createSavingsAccount(account1);
        Assert.assertEquals(ReturnState.NOT_IN_TIME_RANGE.getCode(), savingsAccountResult.getReturnCode());
    }

    @Test
    public void createSavingsAccount_clientNotFoundTest()
    {
        GenericResponse savingsAccountResult = accountManager.createSavingsAccount(account1);
        Assert.assertEquals(ReturnState.CLIENT_NOT_FOUND.getCode(), savingsAccountResult.getReturnCode());
    }

    @Test
    public void createSavingsAccount_wrongInputTest()
    {
        clientRepository.save(client1);

        //Account with wrong account type
        Account account2 = new Account();
        account2.setIdClient(1d);
        account2.setAccountType(AccountType.CHECKING);
        account2.setCurrency(Currency.EUR);
        account2.setAccountNumber("EUR1234");

        GenericResponse savingsAccountResult = accountManager.createSavingsAccount(account2);
        Assert.assertEquals(ReturnState.WRONG_INPUT.getCode(), savingsAccountResult.getReturnCode());
    }
}

