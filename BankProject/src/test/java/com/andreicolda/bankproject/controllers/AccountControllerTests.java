package com.andreicolda.bankproject.controllers;

import com.andreicolda.bankproject.BaseTest;
import com.andreicolda.bankproject.models.Account;
import com.andreicolda.bankproject.models.AccountType;
import com.andreicolda.bankproject.models.Currency;
import com.andreicolda.bankproject.models.ui.GenericResponse;
import com.andreicolda.bankproject.models.ui.ReturnState;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountControllerTests extends BaseTest
{
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void createSavingsAccount_successTest()
    {
        clientRepository.save(client1);

        ResponseEntity<GenericResponse> responseEntity = this.restTemplate
                .postForEntity("/createSavingsAccount", account1, GenericResponse.class);

        assertEquals(200, responseEntity.getStatusCodeValue());
        assertNotNull(responseEntity.getBody());
        assertEquals(ReturnState.SUCCESS.getCode(), responseEntity.getBody().getReturnCode());
    }

    @Test
    void createSavingsAccount_accountLimitExceededTest()
    {
        clientRepository.save(client1);
        accountRepository.save(account1);

        Account account2 = new Account();
        account2.setIdClient(1d);
        account2.setAccountType(AccountType.SAVINGS);
        account2.setCurrency(Currency.RON);
        account2.setBalance(0f);
        account2.setAccountNumber("RON1234");

        ResponseEntity<GenericResponse> responseEntity = this.restTemplate
                .postForEntity("/createSavingsAccount", account1, GenericResponse.class);

        assertEquals(200, responseEntity.getStatusCodeValue());
        assertNotNull(responseEntity.getBody());
        assertEquals(ReturnState.ACCOUNT_LIMIT_EXCEEDED.getCode(), responseEntity.getBody().getReturnCode());
    }

    @Test
    public void createSavingsAccount_notInTimeRangeTest()
    {
        clientRepository.save(client1);

        ResponseEntity<GenericResponse> responseEntity = this.restTemplate
                .postForEntity("/createSavingsAccount", account1, GenericResponse.class);

        assertEquals(200, responseEntity.getStatusCodeValue());
        assertNotNull(responseEntity.getBody());
        assertEquals(ReturnState.NOT_IN_TIME_RANGE.getCode(), responseEntity.getBody().getReturnCode());
    }

    @Test
    public void createSavingsAccount_clientNotFoundTest()
    {
        ResponseEntity<GenericResponse> responseEntity = this.restTemplate
                .postForEntity("/createSavingsAccount", account1, GenericResponse.class);

        assertEquals(200, responseEntity.getStatusCodeValue());
        assertNotNull(responseEntity.getBody());
        assertEquals(ReturnState.CLIENT_NOT_FOUND.getCode(), responseEntity.getBody().getReturnCode());
    }

    @Test
    public void createSavingsAccount_wrongInputTest()
    {
        clientRepository.save(client1);

        //Account with wrong account type
        Account account2 = new Account();
        account2.setIdClient(1d);
        account2.setAccountType(AccountType.CHECKING);
        account2.setCurrency(Currency.EUR);
        account2.setAccountNumber("EUR1234");

        ResponseEntity<GenericResponse> responseEntity = this.restTemplate
                .postForEntity("/createSavingsAccount", account2, GenericResponse.class);

        assertEquals(200, responseEntity.getStatusCodeValue());
        assertNotNull(responseEntity.getBody());
        assertEquals(ReturnState.WRONG_INPUT.getCode(), responseEntity.getBody().getReturnCode());
    }
}
