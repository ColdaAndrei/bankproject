package com.andreicolda.bankproject.services;

import com.andreicolda.bankproject.BaseTest;
import com.andreicolda.bankproject.models.*;
import com.andreicolda.bankproject.models.ui.GenericResponse;
import com.andreicolda.bankproject.models.ui.ReturnState;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.Assert.*;


public class AccountServiceTests extends BaseTest
{
    @Autowired
    private AccountService accountService;

    @Test
    public void createSavingsAccount_successTest()
    {
        Client client = new Client();
        client.setId(1);
        client.setAddress("Address");
        client.setEmail("client1@email.com");
        client.setName("Client Name");
        client.setPersonalIdentifNb("12313213213131");
        client.setTelephone("072123133");

        Account account1 = new Account();
        account1.setIdClient(1d);
        account1.setAccountType(AccountType.SAVINGS);
        account1.setCurrency(Currency.EUR);
        account1.setBalance(0f);
        account1.setAccountNumber("EUR1234");

        clientRepository.save(client);

        GenericResponse savingsAccount = accountService.createSavingsAccount(account1);

        assertEquals(ReturnState.SUCCESS.getCode(), savingsAccount.getReturnCode());
    }

    @Test
    public void createSavingsAccount_failTest()
    {
        Account account1 = new Account();
        account1.setIdClient(1d);
        account1.setAccountType(AccountType.SAVINGS);
        account1.setCurrency(Currency.EUR);
        account1.setBalance(0f);
        account1.setAccountNumber("EUR1234");

        GenericResponse savingsAccount = accountService.createSavingsAccount(account1);

        assertEquals(ReturnState.ERROR.getCode(), savingsAccount.getReturnCode());
    }

    @Test
    public void findSavingsAccountForClient_successTest()
    {
        Client client = new Client();
        client.setId(1);
        client.setAddress("Address");
        client.setEmail("client1@email.com");
        client.setName("Client Name");
        client.setPersonalIdentifNb("12313213213131");
        client.setTelephone("072123133");

        Account account1 = new Account();
        account1.setIdClient(1d);
        account1.setAccountType(AccountType.SAVINGS);
        account1.setCurrency(Currency.EUR);
        account1.setBalance(0f);
        account1.setAccountNumber("EUR1234");

        clientRepository.save(client);

        Optional<Account> findSavingsAccountResult = accountService.findSavingsAccountForClient(account1.getIdClient());

        assertFalse(findSavingsAccountResult.isPresent());

        accountService.createSavingsAccount(account1);

        findSavingsAccountResult = accountService.findSavingsAccountForClient(account1.getIdClient());

        assertTrue(findSavingsAccountResult.isPresent());
    }
}
