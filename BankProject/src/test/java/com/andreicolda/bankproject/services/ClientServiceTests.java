package com.andreicolda.bankproject.services;

import com.andreicolda.bankproject.BaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class ClientServiceTests extends BaseTest
{
    @Autowired
    private ClientService clientService;

    @Test
    public void createSavingsAccount_successTest()
    {
        assertFalse(clientService.findClientById(client1.getId()).isPresent());

        clientRepository.save(client1);
        assertTrue(clientService.findClientById(client1.getId()).isPresent());
    }
}
