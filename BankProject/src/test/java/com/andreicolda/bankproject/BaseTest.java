package com.andreicolda.bankproject;

import com.andreicolda.bankproject.models.Account;
import com.andreicolda.bankproject.models.AccountType;
import com.andreicolda.bankproject.models.Client;
import com.andreicolda.bankproject.models.Currency;
import com.andreicolda.bankproject.repositories.AccountRepository;
import com.andreicolda.bankproject.repositories.ClientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class BaseTest
{
    @Autowired
    protected AccountRepository accountRepository;

    @Autowired
    protected ClientRepository clientRepository;

    protected Client client1;

    protected Account account1;

    @BeforeEach
    void setUp()
    {
        accountRepository.deleteAll();
        clientRepository.deleteAll();

        client1 = new Client();
        client1.setId(1);
        client1.setAddress("Address");
        client1.setEmail("client1@email.com");
        client1.setName("Client Name");
        client1.setPersonalIdentifNb("12313213213131");
        client1.setTelephone("072123133");

        account1 = new Account();
        account1.setIdClient(1d);
        account1.setAccountType(AccountType.SAVINGS);
        account1.setCurrency(Currency.EUR);
        account1.setBalance(0f);
        account1.setAccountNumber("EUR1234");
    }
}